### For local dev:
0) Make sure you have [node](https://nodejs.org/uk/) v17 or latest
1) Install dependencies
```shell
npm i
```
2)Run dev server
```shell
npm run dev
```


## Entry points 
Test crud [http://localhost:3000/admin/personality-test-questions](http://localhost:3000/admin/personality-test-questions)
Public page [http://localhost:3000/](http://localhost:3000/)

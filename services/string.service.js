export function numberToLetter (numeric) {
  return (numeric + 10).toString(36).toUpperCase()
}
